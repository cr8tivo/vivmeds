<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {
        // 
    }

    public function index()
    {
        try{
            return view('dashboard.home');
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }
}
