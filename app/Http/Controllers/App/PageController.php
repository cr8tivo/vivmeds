<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        try{
            $title = "Homepage";

            $data = ['title' => $title];

            return view('pages.index', $data);
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }

    public function transferPage()
    {
        try{
            $title = "Transfer";

            $data = ['title' => $title];

            return view('pages.transferPage', $data);
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }
}
