<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Customers;

class LoadController extends Controller
{
    public function __construct(Customers $customer)
    {
        $this->customers = $customer;
    }

    public function transferPres(Request $request)
    {
        try{
            dd($request->all());
            // Check if email has been stored before, and store if not found
            // Return the id as parameters needed transfering prescription
            $checker = $this->customers->where('email', $request->email)->where('isDeleted', false)->first();
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }
}
