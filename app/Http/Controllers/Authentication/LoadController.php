<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoadController extends Controller
{
    public function __construct()
    {
        // 
    }

    public function login(Request $request)
    {
        try{
            dd($request->all());
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }
}
