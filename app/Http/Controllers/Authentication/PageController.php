<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {
        // 
    }

    public function login()
    {
        try{
            return view('authentication.login');
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }

    public function forgetPassword()
    {
        try{
            return view('authentication.forgetPassword');
            
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }
}
