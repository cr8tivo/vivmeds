<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'email',
        'firstName',
        'lastName',
        'dob',
        'address1',
        'address2',
        'city',
        'state',
        'zipCode',
        'phone',
        'isDeleted'
    ];
}
