@extends('layouts.main')

@section('content')
<style>
	#imp{
		color: red;
	}
</style>
<div class="header_image fontsize40 active">
	Patients Information
</div>

	<div class="container">
		<br><br>
		<div class="row">
			<h3>Fill out our secure form below, and we will certainly get back with you.</h3>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-8">
					<br> <br>
                    <form action="{{url('/transfer/prescription')}}" method="POST">
                    {!! csrf_field() !!}
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label for="">First Name: <b id="imp">*</b></label>
									<input type="text" name="firstName" class="form-control" placeholder="First Name"required>
								</div>
								<div class="col-md-6">
									<label for="">Last Name: <b id="imp">*</b></label>
									<input type="text" name="lastName" class="form-control" placeholder="Last Name"required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-7">
									<label for="">Email Address: <b id="imp">*</b></label>
									<input type="email" name="email" class="form-control" placeholder="Enter Your Email">
								</div>
								<div class="col-md-5">
									<label for="">Date of Birth <b id="imp">*</b></label>
									<input type="date" name="dob" class="form-control" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="">Address: <b id="imp">*</b></label>
							<div class="row">
								<div class="col-md-6">
									<input type="text" name="address1" class="form-control" placeholder="Address 1"required>
								</div>

								<div class="col-md-6">
									<input type="text" name="address2" class="form-control" placeholder="Address 2">
								</div>
							</div>
						</div>

						<div class="form-group">
							
							<div class="row">
								<div class="col-md-6">
									<label for="">City: <b id="imp">*</b></label>
									<input type="text" name="city" class="form-control" placeholder="Enter your city of residence"required>
								</div>

								<div class="col-md-6">
									<label for="">State: <b id="imp">*</b></label>
									<input type="text" name="state" class="form-control" placeholder="Enter your state of residence"required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label for="">Zip Code: <b id="imp">*</b></label>
									<input type="text" name="zipCode" class="form-control" placeholder="Enter Zip Code"required>
								</div>

								<div class="col-md-6">
									<label for="">Telephone Number: <b id="imp">*</b></label>
									<input type="text" name="phone" class="form-control" placeholder="Enter your Telephone number"required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label for="">Previous Pharmacy Name: <b id="imp">*</b></label>
									<input type="text" name="prevPharmName" class="form-control" placeholder="Enter previous pharmacy name"required>
								</div>

								<div class="col-md-6">
									<label for="">Previous Pharmacy Telephone Number: <b id="imp">*</b></label>
									<input type="text" name="prevPharmPhone" class="form-control" placeholder="Enter Number"required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label for="">Prescription Name and Strength <b id="imp">*</b></label>
									<input type="text" name="prescNameStrength" class="form-control" placeholder="Enter response"required>
								</div>

								<div class="col-md-6">
									<label for="">Prescription Number: <b id="imp">*</b></label>
									<input type="text" name="prescNum" class="form-control" placeholder="Enter response"required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success">Submit Transfer</button>
						</div>
					</form>
					<br><br>
				</div>
				<div class="col-md-2" style="padding-top: 60px;">
					<p><a href="https://us.fullscript.com/welcome/vivmeds" target="_top"><img src="https://assets.fullscript.com/buttons/7.jpg" alt="Purchase products through our Fullscript virtual dispensary." border="0"></a></p>
					<p>You can call us for speedy assistance,</p>
					<p>940-226-4849</p>
					<p>940-226-4861</p>
					<p>940-226-4862</p>
					<p></p>
				</div>
			</div>
		</div>
	</div>
@endsection