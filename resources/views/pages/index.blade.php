@extends('layouts.main')

@section('content')
<main>
		<div class="Banner">
			<div class="Banner_shadow"></div>
			<div class="Banner_text">
				<div class="Banner_text_texts">	
					<span class="B_Welcome"> Welcome to <span class="active"> Vivmeds. </span>  </span>
					<div class="B_text">
						We are passionate <br>
						about your wellness
					</div>
					<!-- <span class="B_text"> We are passionate </span> 
					<span class="B_text"> about your wellness </span> -->
					

				</div>
				<div class="Banner_text_button">
					<a href="#Products_Services">
						<button class="Banner_button">
							Products and Services
						</button>
					</a>
				</div>
			</div>
			<div class="Banner_social">
				<div class="B_social_box">
					<div class="Social_box_bar"> </div>
					<div>
						<a href="">
							<span class="fa fa-facebook-official fa-lg white marginTop10"></span>
						</a>
					</div>
					<div> 
						<a href="#">
							<span class="fa fa-twitter-square fa-lg white marginTop10"></span>
						</a>
					</div>
					<div> 
						<a href="#"> 
							<span class="fa fa-instagram fa-lg white marginTop10"></span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="banner_bottom_boxes">
			<div class="Button_Phase1">
				<div class="bottom_box1">
					<div class="height50 width100 alignCent justyCent height70"> 
						<span class="fa fa-refresh- white fontsize70">
							<img src="assets/img/icon/prescription.png" class="width95" style="width: 60%; margin-bottom: -20px;"/> 
						</span> 
					</div>
					<div class="height50 width100 alignCent justyCent height30"> 
						<span class="white fontsize25 text_size">Refill Prescription</span> 
					</div>
				</div>
				<div class="bottom_box2">
					<div class="height50 width100 alignCent justyCent height70"> 
						<span class=" fontsize70 textFont100">
							<img src="assets/img/icon/transfer.png" class="width95" style="width: 60%; margin-bottom: -20px;"/>
						</span> 
					</div>
					<div class="height50 width100 alignCent justyCent height30"> 
						<span class="white fontsize25 text_size"> Transfer a Prescription</span> 
					</div>
				</div>
			</div>
			
			<div class="Button_Phase2">
				<div class="bottom_box3">
					<div class="height50 width100 alignCent justyCent height70"> 
						<span class=" white fontsize70">
							<img src="assets/img/icon/prescription.png" class="width95" style="width: 60%; margin-bottom: -20px;"/> 
						</span> 
					</div>
					<div class="height50 width100 alignCent justyCent height30"> 
						<span class="white fontsize25 text_size"> Auto Prescription</span> 
					</div>
				</div>
				<div class="bottom_box4">
					<div class="height50 width100 alignCent justyCent height70"> 
						<span class=" white fontsize70 textFont100">
							<img src="assets/img/icon/transfer.png" class="width95" style="width: 60%; margin-bottom: -20px;"/>
						</span> 
					</div>
					<div class="height50 width100 alignCent justyCent height30"> 
						<span class="white fontsize25 text_size textalignCen">Schedule a wellness Consultation
						</span> 
					</div>
				</div>
			</div>
		</div>

		<div class="Our_mission" id="Our_mission">
			<div class="Our_mission_boxtext">
				<div class="height50 width100 alignCent justyCent directColm textalgnCent fontsize25 Mission_text1 ">
					<h1 class="Lgreen text_size5"> About Us </h1>
					<div class="text_size"> Vivmeds Pharmacy was founded with the belief of helping our customers achieve their best possible health and lifestyle. Our main priority is the number of positive health outcomes we can help you achieve. <br> Our personable, knowledgeable and well-trained staff will make sure you are not just another prescription. <p> Our continuous innovation, outstanding customer service approach, and sincere dedication to your health makes us the pharmacy that you can always depend on. </p></div>
				</div>
				<div class="height50 width100 alignCent justyCent directColm textalgnCent fontsize25 Mission_text2">
					<h1 class="Lgreen text_size5"> Mission Statement </h1>
					<div class="text_size"> We hope to establish a trusting and long-term relationship with our customers through effective disease state management, and superior consultative services in a compassionate environment.</div>
				</div>
			</div>
		</div>



		<div class="Products_Services" id="Products_Services">
			<div class="Products_Services_text">
				<div class="text_holder">
					<div class="text_holder_text">
						<h1 class=" h1 active text_size5 Prod_ser"> Products <br> and Services</h1>
						<div class="div_text fontsize25 ">
							<P class="text_size">We offer pharmaceutical-grade supplements, high- 
								<br> quality natural and homeopathic remedies in 
								<br> addition to the knowledge customers need for 
								<br> whole health. We listen to and educate our 
								<br> customers.</P>
						</div>
						<div class="div_text2 fontsize25 text_size">
							<p>We have partnered with several wellness vendors 
								<br> to accommodate your unique needs.
							</p>
						</div>
					</div>
					<div class="text_holder_button">
						<button class="text_button text_size">							
							That’s Not All
						</button>
					</div>
				</div>
			</div>
			<div class="Products_Services_img">
				<div class="img_holder">
					<div class="img_holder1">
						<div class="img1">
							<div class="img1Alias">
								<div class="text1">
									<a href="">
										Text goes here
									</a>
								</div>
							</div>
						</div>
						<div class="img2">
							<div class="img2Alias">
								<div class="text2">
									<a href="">
										Text goes here
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="img_holder2">
						<div class="img3">
							<div class="img3Alias">
								<div class="text3">
									<a href="">
										Text goes here
									</a>
								</div>
							</div>
						</div>
						<div class="img4">
							<div class="img4Alias">
								<div class="text4">
									<a href="">
										Text goes here
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="maps" id="maps">
			<div class="direction">
				<div class="line1"> <div class="line"></div> </div>
				<div class="lineText active fontsize70 text_size5"> Get Directions</div>
				<div class="line2"> <div class="lines"></div> </div>
			</div>
			<div class="address fontsize30 text_size"> 3303 Unicorn Lake Blvd Suite 280 Denton TX 76210 </div>
			<div class="map_img"></div>
		</div>

		<div class="Newsletter">
			<div class="Newsletter_text">
				<div class="join fontsize25 text_size3">Join Our Newsletter</div>
				<div class="sign_up fontsize20 text_size"> Sign up today for free and be the first to receive <br> notifications on new updates</div>
			</div>
			<form class="form_control" >
				<div class="email_box">
					<input type="email" class="p_email text_size2" placeholder="Enter Email Address">
				</div>
				<div class="submit_box">
					<button class="submit_button">
						<span class="fa fa-arrow-right white fontsize35"></span>
					</button>
				</div>
			</form>
		</div>
	</main>
@endsection