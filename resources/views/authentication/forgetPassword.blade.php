@extends('layouts.auth')

@section('content')
<!-- Begin page -->
        <!--<div class="accountbg"></div>-->
        <div id="stars"></div>
        <div id="stars2"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-0">
                    <a href="{{url('/')}}" class="logo logo-admin"><img src="{{asset('assets/img/background/home/logo.png')}}" height="70" alt="logo"></a>
                    </h3>

                    <h6 class="text-center">Recover Password</h6>

                    <div class="p-3">
                        <form class="form-horizontal" action="{{url('#')}}">
                        {!! csrf_field() !!}

                            <div class="alert alert-primary alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                Enter your <b>Email</b> and instructions will be sent to you!
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="email" required="" placeholder="Email" name="email">
                                </div>
                            </div>

                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Send Email</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
@endsection