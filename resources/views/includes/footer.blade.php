<footer>
	<div class="footer_div1">
		<div class="div1_box">
			<div class="div1_box1">
				<img src="assets/img/icon/logo.png">
			</div>
			<div class="div1_box2">
				<a href="#">
					<span class="fa fa-facebook-official white fontsize75 marginRight25"></span>
				</a>
				<a href="#">
					<span class="fa fa-twitter-square white fontsize75 marginRight25"></span>
				</a>
				<a href="#">
					<span class="fa fa-instagram white fontsize75"></span>
				</a>
			</div>
		</div>
	</div>
	<div class="footer_div2">
		<div class="div2_box">
			<div class="div2_box_text1 white fontsize25">
				<p class="text_size2">
					3303 Unicorn Lake <br>
					Blvd Suite 280 <br>
					Denton TX 76210 
				</p>
				<br>
				<p class="text_size2">
					940-226-4849 <br>
					940-226-4861 <br>
					940-226-4862
				</p>
				<br>
				<p class="text_size2">
					help@vivmeds.com
				</p>
			</div>
			<div class="div2_box_text2 white">
				<div class="text2_h1 fontsize35 text_size3"> Quick Links </div>
				<div class="About_us white fontsize25">
					<div>
						<p class="text_size2"><a href=""> About Us </a></p>
						<p class="text_size2"><a href=""> Schedule an Appointment </a></p>
						<a href="" class="text_size2"> Transfer a Prescription </a>
					</div>
					<div>
						<p class="text_size2"><a href="#"> Blog </a></p>
						<p class="text_size2"><a href="#"> Schedule an Appointment </a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="prefooter">	</div>
<div class="footer2">
	<div class="footer2_text fontsize30"> &copy 2020 Vivmeds Pharmacy</div>
</div>

@if(Request::is('/'))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header header_height">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true green">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body DisplayFlexCenter">
	          <div class="modal_text">
	            <div class="modal_text_header">
	              Join Our Newsletter
	            </div >
	            <div class="modal_text_body">
	              Sign up today for free and be the first to receive notifications on new updates
	            </div>
	          </div>
	          <form>
	            <div class="form-group">
	              <input type="text" class="form-control send_message" id="recipient-name" placeholder="Enter Email Address">
	            </div>
	          </form>
	        </div>
	        <div class="modal-footer DisplayFlexCenter">
	          <button type="button" class="btn btn-primary my_btn">
	            Send message
	        </button>
	        </div>
	      </div>
	    </div>
	</div>
@endif