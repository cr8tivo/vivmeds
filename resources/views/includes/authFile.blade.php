<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Vivmeds - Admin Area</title>
<meta content="Admin Dashboard" name="description" />
<meta content="themesdesign" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link rel="shortcut icon" href="{{asset('dash-assets/images/favicon.ico')}}">

<link href="{{asset('dash-assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dash-assets/plugins/animate/animate.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dash-assets/css/icons.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dash-assets/css/style.css')}}" rel="stylesheet" type="text/css">