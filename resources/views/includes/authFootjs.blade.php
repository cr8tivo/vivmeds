<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="{{asset('dash-assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dash-assets/js/modernizr.min.js')}}"></script>
<script src="{{asset('dash-assets/js/detect.js')}}"></script>
<script src="{{asset('dash-assets/js/fastclick.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('dash-assets/js/waves.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('dash-assets/js/app.js"></script>