<!-- <header>
		<div class="header_logo">
			<img class="ds" src="assets/img/background/home/logo.png">
		</div>
		<div class="header_links"> 
			<span> <a href="{{url('/')}}" > <span class="active"> HOME </span> </a> </span>
			<span> <a href="{{url('/#Our_mission')}}"> ABOUT US </a> </span>
			<span> <a href="{{url('transfer')}}"> TRANSFER A PRESCRIPTION </a> </span>
			<span> <a href="{{url('/#Products_Services')}}"> PRODUCT & SERVICES </a> </span>
			<span> <a href="{{url('/')}}#"> CONTACT US </a> </span>
		</div>
		<div class="header_icons">
			<button class="btn btn-default btn-bg">Consult</button>
			<!-- <a href="#"> <span class="fa fa-shopping-cart marginR10"> </span> </a>
			<a href="#"> <span class="fa fa-search marginR10"></span> </a>
			<a href="#"> <span class="fa fa-user marginR10"></span> </a> -->
		<!-- </div>
	</header> -->

	<header>
		<div class="header_logo">
			<img class="ds" src="assets/img/background/home/logo.png">
		</div>
		<div class="icon">
			<div class="hamburger"></div>
		</div>

		<nav class="nav">
			<div class="header_links"> 
				<span> <a href="{{url('/')}}" > <span class="active"> HOME </span> </a> </span>
				<span> <a href="{{url('/#Our_mission')}}"> ABOUT US </a> </span>
				<span> <a href="{{url('transfer')}}"> TRANSFER A PRESCRIPTION </a> </span>
				<span> <a href="{{url('/#Products_Services')}}"> PRODUCT & SERVICES </a> </span>
				<span> <a href="{{url('/')}}#"> CONTACT US </a> </span>
			</div>
			<div class="header_icons">
				<button class="header_button">
					Place Order
				</button>
			</div>
		</nav>
	</header> 

	<div class="container_menu">
		<div class="mobile_menu">
			<div>
				<ul>
					<li style="margin-top: 60px;"> 
						<a href="home.html"> <span class="active"> HOME </span> </a> 
					</li>
					<li> 
						<a href=""> ABOUT US</a> 
					</li>
					<li> 
						<a href=""> TRANSFER A PRESCRIPTION </a> 
					</li>
					<li> 
						<a href=""> PRODUCT & SERVICES </a> 
					</li>
					<li> 
						<a href=""> CONTACT US </a> 
					</li>
				</ul>
			</div>
		</div>
	</div>