	<script src="{{ asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('assets/js/home.js')}}"></script>
	

	<script type="text/javascript">

		/*Scroll to top when arrow up clicked BEGIN*/
		$(window).scroll(function() {
		    var height = $(window).scrollTop();
		    if (height > 700) {
		        $('.Back_to_top').fadeIn();
		    } else {
		        $('.Back_to_top').fadeOut();
		    }
		});

		$(document).ready(function() {
		    $(".Back_to_top").click(function(event) {
		        event.preventDefault();
		        $("html, body").animate({ scrollTop: 0 }, "slow");
		        return false;
		    });

		});
 		/*Scroll to top when arrow up clicked END*/
	</script>