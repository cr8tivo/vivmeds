<head>
	<title> {{$title}} </title>
		<link rel="shortcut icon" href="assets/img/icon/fav.png">
	  	<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/flaticons/flaticon.css')}}" rel="stylesheet">
		@if(Request::is('transfer*'))
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/transfer.css')}}"> 
		@endif
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/home.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}"> 
</head>