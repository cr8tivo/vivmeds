<!-- jQuery  -->
<script src="{{asset('dash-assets/js/jquery.min.js')}}"></script>
<script src="{{asset('dash-assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dash-assets/js/modernizr.min.js')}}"></script>
<script src="{{asset('dash-assets/js/detect.js')}}"></script>
<script src="{{asset('dash-assets/js/fastclick.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('dash-assets/js/waves.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('dash-assets/js/jquery.scrollTo.min.js')}}"></script>

<script src="{{asset('dash-assets/plugins/metro/MetroJs.min.js')}}"></script>
<script src="{{asset('dash-assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('dash-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('dash-assets/plugins/sparkline-chart/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('dash-assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('dash-assets/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{asset('dash-assets/pages/dashboard.js')}}"></script>
<!-- App js -->
<script src="{{asset('dash-assets/js/app.js')}}"></script>