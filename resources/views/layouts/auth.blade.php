<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.authFile')
</head>

<body>
    @yield('content')

    @include('includes.authFootjs')
</body>
</html>