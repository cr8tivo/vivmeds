<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.dashHeadFile')
</head>

<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">
    @include('includes.dashSidebar')

    @yield('content')
</div>

    @include('includes.dashFootjs')
</body>
</html>