<!DOCTYPE html>
<html>
    @include('includes.headfile')
    
<body>
    @include('includes.header')

    @yield('content')

    @include('includes.footer')

    <!-- if on homepage show the below section -->

    <button onclick="topFunction()" class="Back_to_top">
			<span class="fa fa-arrow-up white"></span>
    </button>
    
    @include('includes.footjs')

</body>
</html>