@extends('layouts.dashboard')

@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">
                        <nav class="navbar-custom">
                            <ul class="list-inline float-right mb-0">
                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <i class="ti-email noti-icon"></i>
                                        <span class="badge badge-danger noti-icon-badge">5</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5><span class="badge badge-danger float-right">5</span>Messages</h5>
                                        </div>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-2.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Charles M. Jones</b><small class="">Dummy text of the printing and typesetting industry.</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-3.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Thomas J. Mimms</b><small class="">You have 87 unread messages</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-4.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Luis M. Konrad</b><small class="">It is a long established fact that a reader will</small></p>
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <!-- All-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            View All
                                        </a>
                                    </div>
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <i class="ti-bell noti-icon"></i>
                                        <span class="badge badge-success noti-icon-badge">9</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5><span class="badge badge-success float-right">9</span>Notification</h5>
                                        </div>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><small class="">Dummy text of the printing and typesetting industry.</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
                                            <p class="notify-details"><b>New Message received</b><small class="">You have 87 unread messages</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="mdi mdi-martini"></i></div>
                                            <p class="notify-details"><b>Your item is shipped</b><small class="">It is a long established fact that a reader will</small></p>
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <!-- All-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            View All
                                        </a>
                                    </div>
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                        <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5>Welcome</h5>
                                        </div>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle "></i> Profile</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-wallet "></i> My Wallet</a>
                                        <a class="dropdown-item" href="#"><span class="badge badge-primary float-right">3</span><i class="mdi mdi-settings "></i> Settings</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline"></i> Lock screen</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                                    </div>
                                </li>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="float-left">
                                    <button class="button-menu-mobile open-left waves-light waves-effect">
                                        <i class="mdi mdi-menu"></i>
                                    </button>
                                </li>
                                <li class="hide-phone app-search">
                                    <form role="search" class="">
                                        <input type="text" placeholder="Search..." class="form-control">
                                        <a href=""><i class="fa fa-search"></i></a>
                                    </form>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </nav>
                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <div class="float-right">
                                            <div class="dropdown">
                                                <button class="btn btn-secondary btn-round dropdown-toggle px-3" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="mdi mdi-settings mr-1"></i>Settings
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else here</a>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="page-title">Dashboard</h4>
                                    </div>
                                </div>
                            </div>
                            <!-- end page title end breadcrumb -->

                            <div class="row">
                                <div class="col-md-12 col-xl-3">
                                    <div class="card mini-stat">
                                        <div class="mini-stat-icon text-right">
                                            <i class="mdi mdi-cube-outline"></i>
                                        </div>
                                        <div class="p-4">
                                            <h6 class="text-uppercase mb-3">Statistics</h6>
                                            <div class="float-right">
                                                <p class="mb-0"><b>Last:</b> 30.4k</p>
                                            </div>
                                            <h4 class="mb-0">34578<small class="ml-2"><i class="mdi mdi-arrow-up text-primary"></i></small></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xl-3">
                                    <div class="card mini-stat">
                                        <div class="mini-stat-icon text-right">
                                            <i class="mdi mdi-buffer"></i>
                                        </div>
                                        <div class="p-4">
                                            <h6 class="text-uppercase mb-3">User Today</h6>
                                            <div class="float-right">
                                                <p class="mb-0"><b>Last:</b> 1250</p>
                                            </div>
                                            <h4 class="mb-0">895<small class="ml-2"><i class="mdi mdi-arrow-down text-danger"></i></small></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xl-3">
                                    <div class="card mini-stat">
                                        <div class="mini-stat-icon text-right">
                                            <i class="mdi mdi-tag-text-outline"></i>
                                        </div>
                                        <div class="p-4">
                                            <h6 class="text-uppercase mb-3">User This Month</h6>
                                            <div class="float-right">
                                                <p class="mb-0"><b>Last:</b> 40.33k</p>
                                            </div>
                                            <h4 class="mb-0">52410<small class="ml-2"><i class="mdi mdi-arrow-up text-primary"></i></small></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-3">
                                    <div class="card mini-stat">
                                        <div class="mini-stat-icon text-right">
                                            <i class="mdi mdi-briefcase-check"></i>
                                        </div>
                                        <div class="p-4">
                                            <h6 class="text-uppercase mb-3">Request Per Minute</h6>
                                            <div class="float-right">
                                                <p class="mb-0"><b>Last:</b> 956</p>
                                            </div>
                                            <h4 class="mb-0">652<small class="ml-2"><i class="mdi mdi-arrow-down text-danger"></i></small></h4>
                                        </div>
                                    </div>

                                </div>                                            
                            </div><!-- end row -->

                            <div class="row">
                                <div class="col-md-12 col-xl-8">
                                    <div class="card">
                                        <div class="card-body">   
                                            <h4 class="mt-0 mb-3 header-title">Website Overview</h4>        
                                            <div id="morris-area-chart" style="height: 340px;"></div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-12 col-xl-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="mt-0 mb-3 header-title">Monthly Revenue</h4> 
                                            <div id="morris-donut-example" style="height: 340px;"></div>
                                        </div>
                                    </div>
                                </div>
                             
                            </div><!-- end row-->

                            <div class="row">
                                <div class="col-md-12 col-xl-4">
                                    <div class="card">
                                        <div class="card-body">
                                            
                                            <a href="" class="btn btn-light btn-sm float-right">More Info</a>
                                            <h6 class="">Product Status</h6>
                                                                                            
                                            <div id="multi-line-chart" style="height: 350px;"></div>
                                            
                                            <ul class="list-unstyled list-inline text-center mb-0 mt-3">
                                                <li class="list-inline-item">
                                                    <p class="mb-0"><i class="mdi mdi-checkbox-blank-circle mr-2 text-success"></i>Profit</p> 
                                                </li>
                                                <li class="list-inline-item">
                                                    <p class="mb-0"><i class="mdi mdi-checkbox-blank-circle mr-2 text-danger"></i>Lost</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xl-8">
                                    <div class="card">
                                        <div class="card-body">
                                            
                                                
                                            <div class="row">
                                                <div class="col-md-4">   
                                                    <h6 class="mb-0">Website Visitors</h6>                                                  
                                                    <div class=" mt-3">                                                        
                                                        <h3 class="mb-1">528,580<small class="font-12 text-muted  ml-1">last 3 weeks</small></h3>                                                       
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    
                                                    <div class="card">
                                                        <div class="card-body text-center">
                                                            <img src="assets/images/flags/us_flag.jpg" alt="" class="w-30">
                                                            <span class="ml-2">35%<i class="mdi mdi-arrow-down text-danger"></i></span>
                                                            <p class="mb-0 mt-2">Domestic Visits</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="card">
                                                        <div class="card-body text-center">
                                                            <img src="assets/images/flags/russia_flag.jpg" alt="" class="w-30">
                                                            <span class="ml-2">65%<i class="mdi mdi-arrow-up text-primary"></i></span>
                                                            <p class="mb-0 mt-2">International Visits</p>
                                                        </div>
                                                    </div>                                                      
                                                    
                                                </div>
                                                <div class="col-md-12">                                                    
                                                        <div id="world-map-markers" style="height: 291px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            </div><!-- end row -->
                            <div class="row">
                                
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body table-data">
                                            <h6 class="d-inline-block">Overview</h6>
                                            <div class="float-right ml-2">
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-light btn-sm" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="mdi mdi-menu"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                                                        <a class="dropdown-item" href="#">Today</a>
                                                        <a class="dropdown-item" href="#">This Week</a>
                                                        <a class="dropdown-item" href="#">This Month</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th class="border-top-0 w-60">Customers</th>
                                                            <th class="border-top-0">Name</th>
                                                            <th class="border-top-0">Order Date/Time</th>
                                                            <th class="border-top-0">Country</th>
                                                            <th class="border-top-0">Order #</th>
                                                            <th class="border-top-0">Status</th> 
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <img class="rounded-circle" src="assets/images/users/avatar-1.jpg" alt="user" width="40"> 
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="text-dark">James k. Striplin</a>
                                                            </td>
                                                            
                                                            <td>18/05/2018 3:01 PM</td>
                                                            <td>                                                                
                                                                <img src="assets/images/flags/us_flag.jpg" alt="" class="w-30">
                                                            </td>
                                                            <td>9932AZ</td>
                                                            <td><span class="badge badge-boxed  badge-primary">Shipped</span></td>  
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img class="rounded-circle w-50" src="assets/images/users/avatar-2.jpg" alt="user"> </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="text-dark">William A. Johnson</a>
                                                            </td>
                                                            <td>28/05/2018 9:20 AM</td>
                                                            <td>                                                                
                                                                <img src="assets/images/flags/french_flag.jpg" alt="" class="w-30">
                                                            </td>
                                                            <td>2643BZ</td>
                                                            <td><span class="badge badge-boxed  badge-danger">Delivered</span></td> 
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img class="rounded-circle w-50" src="assets/images/users/avatar-3.jpg" alt="user"> </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="text-dark">Bobby M. Gray</a>
                                                            </td>
                                                            <td>3/05/2018 8:45 PM</td>
                                                            <td>                                                                
                                                                <img src="assets/images/flags/spain_flag.jpg" alt="" class="w-30">
                                                            </td>
                                                            <td>2458DA</td>
                                                            <td><span class="badge badge-boxed badge-warning">Pending</span></td> 
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img class="rounded-circle w-50" src="assets/images/users/avatar-4.jpg" alt="user"> </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="text-dark">Sona J. Medrano</a>
                                                            </td>
                                                            <td>25/05/2018 6:35 PM</td>
                                                            <td>                                                                
                                                                <img src="assets/images/flags/russia_flag.jpg" alt="" class="w-30">
                                                            </td>
                                                            <td>7845AA</td>
                                                            <td><span class="badge badge-boxed  badge-primary">Shipped</span> </td> 
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img class="rounded-circle w-50" src="assets/images/users/avatar-5.jpg" alt="user" > 
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="text-dark">Ruby T. Curd</a>
                                                            </td>
                                                            <td>12/05/2018 10:42 AM</td>
                                                            <td>                                                                
                                                                <img src="assets/images/flags/italy_flag.jpg" alt="" class="w-30">
                                                            </td>
                                                            <td>4451CA</td>
                                                            <td><span class="badge badge-boxed badge-warning">Pending</span></td> 
                                                        </tr>                                                      
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>                                    
                                </div> 
                            </div><!-- end row -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © 2018 - 2020 Dashor by Themesdesign.
                </footer>

            </div>
@endsection