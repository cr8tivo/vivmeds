<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.index');
// });

Route::group(['namespace' => 'Authentication'], function(){
    Route::get('login', 'PageController@login');
    Route::post('/login', 'LoadController@login');
    Route::get('/forget/password', 'PageController@forgetPassword');
});

Route::group(['namespace' => 'App'], function(){
    Route::get('/', 'PageController@index');
    Route::get('transfer', 'PageController@transferPage');

    Route::post('transfer/prescription', 'LoadController@transferPres');
});

Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function(){
    Route::get('/home', 'PageController@index');
});